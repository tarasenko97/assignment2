package cse360assign2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


/*
 * Matt Hayes
 * Class ID: 1212994417
 * Assignment 2
 * 
 * This class includes some tests to check the functionalities of the AddingMachine 
 * class. The primary tests include checks for the addition, subtraction, and toString
 * functionalities. The getter method for the class is also tested indirectly.
 */
class AddingMachineTest {

	/*
	 * Test toString, ensure history is saved and outputted.
	 */
	@Test
	void testToString() {
		AddingMachine myCalculator = new AddingMachine();
		myCalculator.add(4);
		myCalculator.subtract(2);
		myCalculator.add(5);
		
		//Note the lack of a trailing space
		assertEquals(myCalculator.toString(), "0 + 4 - 2 + 5");
	}
	
	/*
	 * Test addition, ensure total reflects the addition.
	 */
	@Test
	void testAddition() {
		AddingMachine myCalculator = new AddingMachine();
		myCalculator.add(274);
		
		assertEquals(myCalculator.getTotal(), 274);
	}
	
	/*
	 * Test subtraction, ensure total reflects the subtraction.
	 */
	@Test
	void testSubtraction() {
		AddingMachine myCalculator = new AddingMachine();
		myCalculator.subtract(7);
		
		assertEquals(myCalculator.getTotal(), -7);
	}
	
	/*
	 * Test to ensure the clear method resets total to 0 and history to ""
	 */
	@Test
	void testClear() {
		AddingMachine myCalculator = new AddingMachine();
		myCalculator.add(2);
		myCalculator.clear();
		
		assertEquals(myCalculator.getTotal(), 0);
		assertEquals(myCalculator.toString(), "");
	}
	
	/*
	 * Test to ensure operations work successfully after clearing
	 */
	@Test
	void testOpsAfterClear() {
		AddingMachine myCalculator = new AddingMachine();
		myCalculator.add(2);
		myCalculator.clear();
		myCalculator.subtract(1012);
		
		assertEquals(myCalculator.getTotal(), -1012);
		assertEquals(myCalculator.toString(), "0 - 1012");
	}

}
