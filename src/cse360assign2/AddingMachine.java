package cse360assign2;

/**
 * The AddingMachine class consists of various methods that perform mathematical 
 * operations such as addition and subtraction with other utility methods to
 * show output and prepare for additional operations.
 * 
 * @author Matt Hayes
 * 		   Class ID: 1212994417
 * 		   Assignment 2
 *
 */
public class AddingMachine {

	private int total;
	private String history;
	
	/**
	 * Constructor of the AddingMachine class, sets the class variable total to 0 and
	 * the class variable history to the string "0".
	 * 
	 */
	public AddingMachine () {
		total = 0;  // not needed - included for clarity
		history = "0";
	}
	
	/**
	 * Getter method to return the class variable total.
	 * 
	 * @return The private total variable of the class AddingMachine.
	 */
	public int getTotal () {
		return total;
	}
	
	/**
	 * Method to add the input to the class variable. Also adds the operation of 
	 * addition with the input value to the class string variable history.
	 * 
	 * @param value an integer to be added
	 */
	public void add (int value) {
		total += value;
		
		if(history != "")
			history = history.concat(" + " + value);
		else
			history = history.concat("0 + " + value);
	}
	
	/**
	 * Method to subtract the input from the class variable. Also adds the operation of
	 * subtraction with the input value to the class string variable history.
	 * 
	 * @param value an integer to be subtracted
	 */
	public void subtract (int value) {
		total -= value;
		
		if(history != "")
			history = history.concat(" - " + value);
		else
			history = history.concat("0 - " + value);
	}
		
	/**
	 * Method to return a string consisting of the history of math operations done.
	 * 
	 * @return The class string variable history which holds the history of operations.
	 */
	public String toString () {
		return history;
	}

	/**
	 * Method to clear total to 0 and history to ""
	 */
	public void clear() {
		total = 0;
		history = "";
	}
}
